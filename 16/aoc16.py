"""
Advent of Code, day 16
"""

# Directions are characters: N E S W

# map feature -> direction in -> direction out
# direction-in means light direction, so it came from the
# "reversed" cell.
CELL_DIRS = {
    ".": {"N": "N", "E": "E", "S": "S", "W": "W"},
    "/": {"N": "E", "E": "N", "S": "W", "W": "S"},
    "\\": {"N": "W", "E": "S", "S": "E", "W": "N"},
    "-": {"N": "EW", "E": "E", "S": "EW", "W": "W"},
    "|": {"N": "N", "E": "NS", "S": "S", "W": "NS"},
}

# If light is going in a direction, what new r/c does it hit.
DIR_TO_RC = {
    "N": (-1, 0),
    "E": (0, 1),
    "S": (1, 0),
    "W": (0, -1),
}


class Grid:
    def __init__(self, rows):
        self.rows = rows
        self.nrows = len(rows)
        self.ncols = len(rows[0])
        # Record light by where it came *into* a cell.
        self.reset_light()

    def reset_light(self):
        self._light = [[[] for _ in range(self.ncols)] for _ in range(self.nrows)]

    @classmethod
    def read(cls, path):
        with open(path) as f:
            return cls([line.strip() for line in f])

    def scan(self, r, c, dir_in):
        if r < 0 or r >= self.nrows or c < 0 or c >= self.ncols:
            return
        if dir_in in self._light[r][c]:
            return
        self._light[r][c].append(dir_in)
        cell = self.rows[r][c]
        for dir_out in CELL_DIRS[cell][dir_in]:
            dr, dc = DIR_TO_RC[dir_out]
            self.scan(r + dr, c + dc, dir_out)

    def scan_iter(self, r, c, dir_in):
        stack = [(r, c, dir_in)]
        while stack:
            r, c, dir_in = stack.pop()
            if r < 0 or r >= self.nrows or c < 0 or c >= self.ncols:
                continue
            if dir_in in self._light[r][c]:
                continue
            self._light[r][c].append(dir_in)
            cell = self.rows[r][c]
            for dir_out in CELL_DIRS[cell][dir_in]:
                dr, dc = DIR_TO_RC[dir_out]
                stack.append((r + dr, c + dc, dir_out))

    def scan0(self):
        # initial direction (part 1)
        self.scan(0, 0, "E")

    def active(self):
        return sum(sum(1 if dirs else 0 for dirs in row) for row in self._light)

    def scan_all(self):
        max_active = 0

        def do_it(r, c, dir_in):
            nonlocal max_active
            self.reset_light()
            self.scan_iter(r, c, dir_in)
            max_active = max(max_active, self.active())

        # top / bottom row
        for c in range(self.ncols):
            do_it(0, c, "S")
            do_it(self.nrows - 1, c, "N")
        # left / right columns
        for r in range(self.nrows):
            do_it(r, 0, "E")
            do_it(r, self.ncols - 1, "W")
        return max_active
