"""
Advent of code day 14
"""

import time


def read_grid(path):
    with open(path) as f:
        # Strip newline, drop empty.
        lines = [x for x in [x.strip() for x in f.readlines()] if x]
    return [list(line) for line in lines]


def copy_grid(grid):
    return [row[:] for row in grid]


def roll_north(grid):
    grid = copy_grid(grid)
    return roll_north_(grid)


# in place!
def roll_north_(grid):
    for col in range(len(grid[0])):
        roll_north_col_(grid, col)
    return grid


def roll_south_(grid):
    for col in range(len(grid[0])):
        roll_south_col_(grid, col)
    return grid


def roll_east_(grid):
    for row in range(len(grid)):
        roll_east_row_(grid, row)
    return grid


def roll_west_(grid):
    for row in range(len(grid)):
        roll_west_row_(grid, row)
    return grid


def cycle(grid):
    grid = copy_grid(grid)
    return cycle_(grid)


def cycle_(grid):
    roll_north_(grid)
    roll_west_(grid)
    roll_south_(grid)
    roll_east_(grid)
    return grid


def n_cycle(grid, n):
    t0 = time.time()
    prev_grids = []
    grid = copy_grid(grid)
    for i in range(n):
        prev_grid = copy_grid(grid)
        prev_grids.append(prev_grid)
        cycle_(grid)
        if grid in prev_grids:  # grid == prev_grid:
            print("repeat after", i)
            idx = prev_grids.index(grid)
            print("index is", idx)
            # grid is cycle^(i+1) g0  (i=0 is 1 iteration)
            # prev is cycle^(idx) g0
            # Therefore the cycle length is:
            cycle = i + 1 - idx
            # And the index at n is:
            idx_n = idx + (n - idx) % cycle
            # So if we replace (n-1) with i, we get
            # idx_n = idx.
            return prev_grids[idx_n]
        if i % 50_000 == 0:
            print(i, time.time() - t0)
    return grid


# In-place!
def roll_north_col_(grid, c):
    first_space = None
    for r, row in enumerate(grid):
        ch = row[c]
        if ch == "#":
            first_space = None
        elif ch == ".":
            if first_space is None:
                first_space = r
        elif ch == "O":
            if first_space is not None:
                grid[first_space][c] = "O"
                row[c] = "."
                first_space += 1


def roll_south_col_(grid, c):
    last_space = None
    for r in range(len(grid) - 1, -1, -1):
        row = grid[r]
        ch = row[c]
        if ch == "#":
            last_space = None
        elif ch == ".":
            if last_space is None:
                last_space = r
        elif ch == "O":
            if last_space is not None:
                grid[last_space][c] = "O"
                row[c] = "."
                last_space -= 1


def roll_east_row_(grid, r):
    last_space = None
    row = grid[r]
    for c in range(len(row) - 1, -1, -1):
        ch = row[c]
        if ch == "#":
            last_space = None
        elif ch == ".":
            if last_space is None:
                last_space = c
        elif ch == "O":
            if last_space is not None:
                row[last_space] = "O"
                row[c] = "."
                last_space -= 1


def roll_west_row_(grid, r):
    first_space = None
    row = grid[r]
    for c in range(len(row)):
        ch = row[c]
        if ch == "#":
            first_space = None
        elif ch == ".":
            if first_space is None:
                first_space = c
        elif ch == "O":
            if first_space is not None:
                row[first_space] = "O"
                row[c] = "."
                first_space += 1


def grid_load(grid):
    nrow = len(grid)
    tot = 0
    # for c in range(len(grid[0])):
    #     for r, row in enumerate(grid):
    #         if row[c] == 'O':
    #             tot += nrow - r
    #         else:
    #             break
    for r, row in enumerate(grid):
        tot += (nrow - r) * row.count("O")
    return tot
