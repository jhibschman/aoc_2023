import aoc12

import unittest

class Aoc12Test(unittest.TestCase):
    def test_parse(self):
        row, counts = aoc12.parse_line('???.### 1,1,3')
        self.assertEqual(row, '???.###')
        self.assertEqual(counts, [1, 1, 3])

if __name__ == '__main__':
    unittest.main()
