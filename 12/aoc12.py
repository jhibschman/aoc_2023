"""
Advent of Code, day 12.
"""

import collections
import dataclasses
import time
from typing import Optional

# First row that the solver got stuck on.
hard_line = "?????.??##?????????. 2,6,2"


def read_board(path):
    return open(path).read()


def solve_board(board, version=1):
    return solve_row_counts(parse_board(board), version=version)


def solve_big_board(board, version=1):
    big_data = [embiggen(r, c) for r, c in parse_board(board)]
    return solve_row_counts(big_data, version=version)


def embiggen(row, counts):
    big_row = "?".join([row] * 5)
    big_counts = counts * 5
    return big_row, big_counts


def solve_row_counts(row_counts, version=1):
    t0 = time.time()
    tot, res = 0, 0
    for i, (row, counts) in enumerate(row_counts):
        print(f"{i}/{len(row_counts)}: {res} -> {tot} - {time.time() - t0:.1f}")
        if version == 1:
            res = find_solutions(row, counts)
        elif version == 2:
            res = find_solutions2(row, counts)
        elif version == 3:
            res = find_solutions3(row, counts)
        else:
            raise RuntimeError(f"unknown version: {version}")
        tot += res
    print(
        f"{len(row_counts)}/{len(row_counts)}: {res} -> {tot} - {time.time() - t0:.1f}"
    )
    return tot


def parse_board(board):
    return [parse_line(line) for line in board.split("\n") if line]


def parse_line(line):
    """Parse a line into the row itself and counts."""
    row, count_line = line.split()
    bad_counts = [int(x) for x in count_line.split(",")]
    return row, bad_counts


def row_stats(row):
    bad = row.count("#")
    unknown = row.count("?")
    good = row.count(".")
    return {"bad": bad, "unknown": unknown, "good": good}


# Get the big-size of the difficult row.
hard_row, hard_counts = embiggen(*parse_line(hard_line))


@dataclasses.dataclass
class State:
    row_i: int
    count_i: int  # may == len(counts)
    cur_run: int
    bad_left: int


NState = collections.namedtuple(
    "NState", ["row_i", "count_i", "cur_run", "bad_left", "good_left"]
)

State3 = collections.namedtuple("State3", ["row_i", "count_i", "cur_run"])


def find_solutions3(row, counts):
    """Alternative, that recurses and caches, using simpler state."""
    # state = (row_i, counts_i, cur_run)  -- State3
    # counts_i can be len(counts).
    nrow, ncounts = len(row), len(counts)
    cache = {}

    def check_end_state(st):
        """Return either 0, if it's not right, or 1, if wrong."""
        if st.row_i != nrow:
            raise RuntimeError("not at end state")
        if st.cur_run == 0:
            return int(st.count_i == ncounts)
        else:
            return int(st.count_i == ncounts - 1 and st.cur_run == counts[-1])

    def count_configs(st):
        # Check the cache.
        if st in cache:
            return cache[st]
        row_i, count_i, cur_run = st
        if row_i == nrow:
            return check_end_state(st)

        # scan to the next ?.
        # go char-by-char to handle cur_run.
        while row_i < nrow and row[row_i] != "?":
            ch = row[row_i]
            if ch == "." and cur_run > 0:
                if count_i < ncounts and counts[count_i] == cur_run:
                    cur_run = 0
                    count_i += 1
                else:
                    return 0
            elif ch == "#":
                cur_run += 1
                if count_i == ncounts or cur_run > counts[count_i]:
                    return 0
            row_i += 1
        if row_i == nrow:  # at the end?
            return check_end_state(State3(row_i, count_i, cur_run))
        # row[row_i] == '?', now do the choice.
        result = count_configs(State3(row_i + 1, count_i, cur_run + 1))  # ? -> #
        if cur_run == 0:
            result += count_configs(State3(row_i + 1, count_i, 0))  # ? -> .
        elif count_i < ncounts and counts[count_i] == cur_run:
            result += count_configs(State3(row_i + 1, count_i + 1, 0))  # ? -> .
        cache[st] = result
        return result

    return count_configs(State3(0, 0, 0))


def find_solutions2(row, counts):
    """Alternative, that recurses and caches."""
    stats = row_stats(row)
    bad, unk, good = stats["bad"], stats["unknown"], stats["good"]
    bad_tot = sum(counts)
    good_tot = len(row) - bad_tot
    bad_left = bad_tot - bad  # how many ?s must convert to #
    good_left = bad_tot - bad  # how many ?s must convert to .
    row_size = len(row)
    counts_size = len(row)

    cache = {}

    def count_configs(st):
        # Quick tests to see if we're done.
        # These are to avoid letting the cache get too big.
        if st.row_i == row_size:
            if st.cur_run == 0:
                return int(st.count_i == len(counts))
            else:
                return int(st.count_i == len(counts) - 1 and counts[-1] == st.cur_run)
        # Check the cache.
        if st in cache:
            return cache[st]
        row_i, count_i, cur_run, bad_left, good_left = st
        # scan to the next ?.
        result = None
        while row_i < row_size and row[row_i] != "?":
            if row[row_i] == ".":
                if cur_run > 0:
                    if (count_i == counts_size) or (counts[count_i] != cur_run):
                        result = 0
                        break
                    cur_run = 0
                    count_i += 1
                else:
                    pass
            elif count_i == counts_size:
                result = 0
                break
            else:
                cur_run += 1
                if cur_run > counts[count_i]:
                    result = 0
                    break
            row_i += 1
        if row_i == row_size:  # at the end
            # invoke the check-if-ok logic.
            result = count_configs(NState(row_i, count_i, cur_run, bad_left, good_left))
        if result is not None:
            cache[st] = result
            return result
        # Now do the choice.
        result = 0
        if bad_left:
            result += count_configs(
                NState(row_i + 1, count_i, cur_run + 1, bad_left - 1, good_left)
            )
        if good_left:
            if cur_run > 0:
                if count_i < counts_size and cur_run == counts[count_i]:
                    result += count_configs(
                        NState(row_i + 1, count_i + 1, 0, bad_left, good_left - 1)
                    )
            else:
                result += count_configs(
                    NState(row_i + 1, count_i + 1, 0, bad_left, good_left - 1)
                )
        cache[st] = result
        return result

    return count_configs(NState(0, 0, 0, bad_left, good_left))


def find_solutions(row, counts):
    # slow, bad solution.
    stats = row_stats(row)
    bad, unk, good = stats["bad"], stats["unknown"], stats["good"]
    bad_tot = sum(counts)
    good_tot = len(row) - bad_tot
    stack = [NState(0, 0, 0, bad_tot - bad, good_tot - good)]
    success = 0

    # These don't update the "*l_eft" fields, that's in the ?-handler.
    def maybe_good(row_i, count_i, cur_run, bad_left, good_left):
        if cur_run > 0:
            # End the current run, must equal the next count.
            if count_i == len(counts) or cur_run != counts[count_i]:
                return
            stack.append(NState(row_i + 1, count_i + 1, 0, bad_left, good_left))
        else:
            stack.append(NState(row_i + 1, count_i, 0, bad_left, good_left))

    def maybe_bad(row_i, count_i, cur_run, bad_left, good_left):
        new_run = cur_run + 1
        if count_i == len(counts) or new_run > counts[count_i]:
            # Too many in the current run, fail.
            return
        stack.append(NState(row_i + 1, count_i, new_run, bad_left, good_left))

    count = 0
    while stack:
        count += 1
        row_i, count_i, cur_run, bad_left, good_left = stack.pop()
        # if count % 100000 == 0:
        #     print(count, len(stack), row_i, count_i, cur_run, bad_left, good_left)
        if row_i == len(row):
            if cur_run > 0 and count_i == len(counts) - 1 and counts[-1] == cur_run:
                success += 1
            if cur_run == 0 and count_i == len(counts):
                success += 1
            continue
        ch = row[row_i]
        if ch == ".":
            maybe_good(row_i, count_i, cur_run, bad_left, good_left)
        elif ch == "#":
            maybe_bad(row_i, count_i, cur_run, bad_left, good_left)
        elif ch == "?":
            # choice!
            if good_left > 0:
                maybe_good(row_i, count_i, cur_run, bad_left, good_left - 1)
            if bad_left > 0:
                maybe_bad(row_i, count_i, cur_run, bad_left - 1, good_left)
        else:
            raise RuntimeError("illegal char: " + ch)
    return success
