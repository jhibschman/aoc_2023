"""
Advent of Code, day 17.

- At most three times in the same direction in a row.
- Can't turn 180 degrees, just left or right.
- Find the path with the least heat loss from upper-left to lower-right.

Thoughts:
- Dijkstra's path algorithm would work, if no constraints.
- Dynamic programming; well, caching.
- Full state is (r, c, dir, dir_times, path_total), which is big.

Well, honestly, dir*dir_times is only 12, so I should be able to
check it all.
"""

import heapq

DIRS = "NSEW"
OPPOSITES = {"N": "S", "S": "N", "E": "W", "W": "E"}
NEXT_DIRS = {d: [nd for nd in DIRS if nd != OPPOSITES[d]] for d in DIRS}
DIR_DELTA = {"N": (-1, 0), "S": (1, 0), "E": (0, 1), "W": (0, -1)}


class Grid:
    def __init__(self, rows):
        # rows is a list of list of ints.
        self.rows = rows
        self.nrow = len(rows)
        self.ncol = len(rows[0])
        self.reset()

    def reset(self):
        # (r,c)->(d, dt)->Optional[int]
        # path cost includes the cost of getting there.
        # self.best = [[{} for _ in self.ncol] for _ in self.nrow]
        # cost includes the current cell.
        self.best = {}

    @classmethod
    def read(cls, path):
        with open(path) as f:
            lines = [line.strip() for line in f]
        data = [[int(ch) for ch in line] for line in lines]
        return cls(data)

    def in_bounds(self, r, c):
        return r >= 0 and r < self.nrow and c >= 0 and c < self.ncol

    def already_done(self, state, cost):
        return (prev_best := self.best.get(state)) and prev_best <= cost

    def search(self):
        # state = (
        #  (row, column, prev direction, times moved that direction),
        #   path cost including the current cell
        #  ).
        heap = [(0, (0, 0, "E", 0))]
        best_final = None
        while heap:
            cost, state = heapq.heappop(heap)
            r, c, d, dt = state
            if not self.in_bounds(r, c):
                continue
            # Update the current state with the cost.
            if self.already_done(state, cost):
                continue
            self.best[state] = cost
            # If we're at the end, there's no next move.
            if r == self.nrow - 1 and c == self.ncol - 1:
                best_final = min(best_final, cost) if best_final else cost
                continue
            # Otherwise, compute the legal moves.
            for d2 in NEXT_DIRS[d]:
                dt2 = dt + 1 if d2 == d else 1
                if dt2 > 3:
                    continue
                dr, dc = DIR_DELTA[d2]
                r2, c2 = r + dr, c + dc
                if not self.in_bounds(r2, c2):
                    continue
                state2 = (r2, c2, d2, dt2)
                cost2 = cost + self.rows[r2][c2]
                if self.already_done(state2, cost2):
                    continue
                heapq.heappush(heap, (cost2, state2))
        return best_final

    def search_ultra(self):
        # state = (
        #  (row, column, prev direction, times moved that direction),
        #   path cost including the current cell
        #  ).
        heap = [(0, (0, 0, "E", 0))]
        best_final = None
        while heap:
            cost, state = heapq.heappop(heap)
            r, c, d, dt = state
            if not self.in_bounds(r, c):
                continue
            # Update the current state with the cost.
            if self.already_done(state, cost):
                continue
            self.best[state] = cost
            # If we're at the end, there's no next move.
            if r == self.nrow - 1 and c == self.ncol - 1:
                # We can only stop if we've moved 4 so far.
                if dt >= 4:
                    best_final = min(best_final, cost) if best_final else cost
                continue
            # Otherwise, compute the legal moves.
            for d2 in NEXT_DIRS[d]:
                if dt < 4 and d2 != d:
                    # need to move at least 4 before turning
                    continue
                dt2 = dt + 1 if d2 == d else 1
                if dt2 > 10:
                    # gone too far
                    continue
                dr, dc = DIR_DELTA[d2]
                r2, c2 = r + dr, c + dc
                if not self.in_bounds(r2, c2):
                    continue
                state2 = (r2, c2, d2, dt2)
                cost2 = cost + self.rows[r2][c2]
                if self.already_done(state2, cost2):
                    continue
                heapq.heappush(heap, (cost2, state2))
        return best_final
