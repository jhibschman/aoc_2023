"""
Advent of Code, day 19.

Rules for sorting parts.

Rule files are a set of rules, a blank space, then the parts to sort.
"""

from collections.abc import Iterable
import operator
import re
from typing import Optional


def parse_data_path(path):
    with open(path) as f:
        return parse_data(f)


def parse_data(lines: Iterable[str]):
    """Parses a data file of rules and parts."""
    rules, parts = [], []
    for line in lines:
        line = line.rstrip()
        if not line:
            break
        # print('line', line)
        rules.append(parse_rule(line))
    for line in lines:
        line = line.rstrip()
        parts.append(parse_part(line))
    return RuleSet(rules), parts


def parse_rule(line: str):
    """Parse a rule.
    Example: px{a<2006:qkq,m>2090:A,rfg}
    """
    brace = line.index('{')
    rule_name = line[:brace]
    clause_text = line[brace+1:-1].split(',')
    clauses = [parse_rule_clause(clause) for clause in clause_text]
    return Rule(rule_name, clauses)


def parse_rule_clause(clause: str):
    if ':' not in clause:
        return RCTerminal(clause)
    cond, dest = clause.split(':')
    # cond is e.g. a<2006.
    parts = re.split(r'([<>])', cond)
    assert len(parts) == 3
    return RCCondition(parts[0], parts[1], int(parts[2]), dest)



class RuleSet:
    def __init__(self, rules):
        self.rules = {rule.name: rule for rule in rules}
    def accept(self, part) -> bool:
        cur_state = 'in'
        while cur_state not in ('A', 'R'):
            cur_state = self.rules[cur_state].apply(part)
        return cur_state == 'A'
    def apply_range(self, part_range=None):
        results = []
        stack = [('in', part_range or PartRange())]
        while stack:
            print(stack)
            name, rng = stack.pop()
            res = self.rules[name].apply_range(rng)
            results.extend([(n, r) for n, r in res if n in ('A', 'R')])
            stack.extend([(n, r) for n, r in res if n not in ('A', 'R')])
        return results
    def accept_range(self, part_range=None):
        return [pr for n, pr in self.apply_range(part_range) if n == 'A']
    def accept_range_total(self, part_range=None):
        return sum(pr.combos() for pr in self.accept_range(part_range))


class Rule:
    def __init__(self, name, clauses):
        self.name = name
        self.clauses = clauses
    def apply(self, part):
        for clause in self.clauses:
            if result := clause.apply(part):
                return result
        raise RuntimeError('no matching clause')
    def apply_range(self, part_range):
        """Apply a rule to a range. Returns a dict from result
        to parts.
        Result is alist of (destination, range).
        """
        results = []
        for clause in self.clauses:
            dest, dest_range, rem_range = clause.apply_range(part_range)
            if dest_range is not None:
                results.append((dest, dest_range))
            if rem_range is None:
                return results
            part_range = rem_range
        raise RuntimeError('no matching clause')


class RCTerminal:
    """Terminal rule clause, either A, R, or a label."""
    def __init__(self, result):
        self.result = result

    def apply(self, part):
        del part
        return self.result

    def apply_range(self, part_range):
        return self.result, part_range, None


class RCCondition:
    """Condition-test rule clause."""
    def __init__(self, field, op, value, result):
        self.field = field
        if op == '<':
            self.op = operator.lt
        elif op == '>':
            self.op = operator.gt
        else:
            raise RuntimeError('bad operator')
        self.value = value
        self.result = result

    def apply(self, part) -> Optional[str]:
        if self.op(part[self.field], self.value):
            return self.result
        else:
            return None
        
    def apply_range(self, part_range):
        """Apply a rule to a range.
        Returns None if the rule never applies, otherwise return a
        new range.
        """
        if self.op is operator.lt:
            yes, no = part_range.split_lt(self.field, self.value)
        else:
            yes, no = part_range.split_gt(self.field, self.value)
        return self.result, yes, no


class PartRange:
    def __init__(self, ranges=None):
        ranges = ranges or [(1, 4000)] * 4
        self.x = ranges[0]
        self.m = ranges[1]
        self.a = ranges[2]
        self.s = ranges[3]
    def get(self, field):
        return getattr(self, field)  # ugly
    def split_lt(self, field, value):
        fmin, fmax = self.get(field)
        highest_included = min(fmax, value - 1)
        r1 = self.update_range(field, (fmin, highest_included))
        r2 = self.update_range(field, (highest_included + 1, fmax))
        return r1, r2
    def split_gt(self, field, value):
        fmin, fmax = self.get(field)
        lowest_included = max(fmin, value + 1)
        r1 = self.update_range(field, (lowest_included, fmax))
        r2 = self.update_range(field, (fmin, lowest_included - 1))
        return r1, r2
    def update_range(self, field, new_range):
        if new_range[0] > new_range[1]:
            return None
        i = 'xmas'.find(field)
        ranges = [self.x, self.m, self.a, self.s]
        ranges[i] = new_range
        return PartRange(ranges)
    def combos(self):
        def rsize(r):
            return r[1] - r[0] + 1
        return rsize(self.x) * rsize(self.m) * rsize(self.a) * rsize(self.s)
    def __repr__(self):
        return f'{{x={self.x},m={self.m},a={self.a},s={self.s}}}'

def parse_part(line: str):
    """Parse a part.
    Example: {x=787,m=2655,a=1222,s=2876}
    """
    if line[0] != '{' or line[-1] != '}':
        raise RuntimeError('badly-formed part')
    part = {}
    for clause in line[1:-1].split(','):
        name_val = clause.split('=')
        part[name_val[0]] = int(name_val[1])
    return part

def score(part):
    return part['x'] + part['m'] + part['a'] + part['s']

def score_accepted(rules, parts):
    return sum(score(part) for part in parts if rules.accept(part))


# Test code
test_rules, test_parts = parse_data_path('test_data.txt')
rules, parts = parse_data_path('data.txt')
print(score_accepted(rules, parts))
