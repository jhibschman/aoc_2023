"""
Advent of Code, day 11.
"""

import sys


def read_grid(path):
    with open(path) as f:
        return [line.strip() for line in f.readlines()]


def collect_data(grid):
    row_counts = [0] * len(grid)
    col_counts = [0] * len(grid[0])
    galaxies = []
    for r, row in enumerate(grid):
        for c, ch in enumerate(row):
            if ch != "#":
                continue
            row_counts[r] += 1
            col_counts[c] += 1
            galaxies.append((r, c))
    return galaxies, row_counts, col_counts


def counts_to_metric(counts, expansion):
    metric = [0] * len(counts)
    dist = 0
    for i, c in enumerate(counts):
        metric[i] = dist
        dist += expansion if c == 0 else 1
    return metric


def total_distance(galaxies, row_metric, col_metric):
    dist = 0
    for i, (r1, c1) in enumerate(galaxies):
        for j in range(i + 1, len(galaxies)):
            r2, c2 = galaxies[j]
            dist += abs(row_metric[r2] - row_metric[r1])
            dist += abs(col_metric[c2] - col_metric[c1])
    return dist


def do_grid(grid, expansion):
    galaxies, row_counts, col_counts = collect_data(grid)
    row_metric = counts_to_metric(row_counts, expansion)
    col_metric = counts_to_metric(col_counts, expansion)
    dist = total_distance(galaxies, row_metric, col_metric)
    print(dist)


def do_path(path, expansion):
    grid = read_grid(path)
    do_grid(grid)


def main():
    grid = read_grid(sys.argv[1])
    do_grid(grid, 2)
    do_grid(grid, 1_000_000)


if __name__ == "__main__":
    main()
