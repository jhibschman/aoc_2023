"""
Advent of code day 13
"""


def read_grids(path):
    grids, grid = [], []
    with open(path) as f:
        for line in f:
            line = line.rstrip()
            if not line:
                grids.append(grid)
                grid = []
            else:
                grid.append(line)
        grids.append(grid)
    return grids


def read_grid(path):
    with open(path) as f:
        # Strip newline, drop empty.
        return [x for x in [x.strip() for x in f.readlines()] if x]


def transpose(lines):
    nrow = len(lines)
    ncol = len(lines[0])
    result = []
    for c in range(ncol):
        new_line = "".join(line[c] for line in lines)
        result.append(new_line)
    return result


def find_mirror(lines):
    # separator is between rows sep-1 and sep.
    for sep in range(1, len(lines)):
        if all(
            lines[sep - 1 - i] == lines[sep + i]
            for i in range(min(sep, len(lines) - sep))
        ):
            return sep
    return None


def find_mirror_smudge(lines):
    # separator is between rows sep-1 and sep.
    for sep in range(1, len(lines)):
        smudge_seen, bad = False, False
        for i in range(min(sep, len(lines) - sep)):
            diffs = sum(a != b for a, b in zip(lines[sep - 1 - i], lines[sep + i]))
            if diffs == 1:
                if smudge_seen:
                    bad = True
                    break
                smudge_seen = True
            elif diffs > 1:
                bad = True
                break
        if smudge_seen and not bad:
            return sep
    return None


def find_score(grids):
    return sum(find_score_grid(g) for g in grids)


def find_score_smudge(grids):
    return sum(find_score_grid_smudge(g) for g in grids)


def find_score_grid(grid):
    row = find_mirror(grid)
    if row:
        return 100 * row
    col = find_mirror(transpose(grid))
    if col:
        return col
    return 0


def find_score_grid_smudge(grid):
    row = find_mirror_smudge(grid)
    if row:
        return 100 * row
    col = find_mirror_smudge(transpose(grid))
    if col:
        return col
    return 0
