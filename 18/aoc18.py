"""
Advent of code, day 18.

Analyze the dig plan for a lagoon.

"""

import dataclasses
from typing import Optional


def read_dig_plan(path):
    with open(path) as f:
        return [parse_dig_line(line) for line in f]


@dataclasses.dataclass
class DigLine:
    direction: str
    length: int
    color: str


def parse_dig_line(line) -> Optional[DigLine]:
    parts = line.split()
    if len(parts) != 3:
        return None
    return DigLine(parts[0], int(parts[1]), parts[2][1:-1])

def reparse(dig_line):
    """Reparse the dig line from the color."""
    d = "RDLU"[int(dig_line.color[-1])]
    n = int(dig_line.color[1:-1], 16)
    return DigLine(d, n, '')

class Lagoon:
    """Simple, just store all the data densely."""

    def __init__(self):
        self.data = [[1]]
        self.row_min = 0
        self.row_max = 1  # exclusive
        self.col_min = 0
        self.col_max = 1  # exclusive
        self.pos = (0, 0)
        # track edge color?

    def dig(self, dig_line):
        r, c = self.pos
        dr, dc = {"R": (0, 1), "L": (0, -1), "U": (-1, 0), "D": (1, 0)}[
            dig_line.direction
        ]
        rn, cn = r + dig_line.length * dr, c + dig_line.length * dc
        self._ensure_space(rn, cn)
        if dr:
            for i in range(1, dig_line.length + 1):
                self.set(r + i * dr, c, 1)
        if dc:
            for i in range(1, dig_line.length + 1):
                self.set(r, c + i * dc, 1)
        self.pos = (rn, cn)

    def in_bounds(self, r, c):
        return (
            r >= self.row_min
            and r < self.row_max
            and c >= self.col_min
            and c < self.col_max
        )

    def set(self, r, c, val):
        self.data[r - self.row_min][c - self.col_min] = val

    def get(self, r, c):
        return self.data[r - self.row_min][c - self.col_min]

    def dig_all(self, dig_lines):
        for dig_line in dig_lines:
            self.dig(dig_line)

    def _ensure_space(self, r, c):
        if c >= self.col_max:
            new_cols = max(c - self.col_max + 1, 10)
            for row in self.data:
                row.extend([0] * new_cols)
            self.col_max += new_cols
        if c < self.col_min:
            new_cols = max(self.col_min - c, 10)
            for i in range(len(self.data)):
                self.data[i] = [0] * new_cols + self.data[i]
            self.col_min -= new_cols
        if r >= self.row_max:
            new_rows = max(r - self.row_max + 1, 10)
            col_len = len(self.data[0])
            self.data = self.data + [[0] * col_len for _ in range(new_rows)]
            self.row_max += new_rows
        if r < self.row_min:
            new_rows = max(self.row_min - r, 10)
            col_len = len(self.data[0])
            self.data = [[0] * col_len for _ in range(new_rows)] + self.data
            self.row_min -= new_rows

    def fill(self):
        """Fill in the ditches."""
        # do I need to do flood-fill?
        # How do I distiguish two up/down walls from left-right walls?
        # How about corners?
        stack = [(1, 1)]
        while stack:
            r, c = stack.pop()
            if not self.in_bounds(r, c):
                continue
            if self.get(r, c):
                continue
            self.set(r, c, 1)
            stack.append((r, c + 1))
            stack.append((r, c - 1))
            stack.append((r + 1, c))
            stack.append((r - 1, c))

    def count_holes(self):
        return sum(sum(row) for row in self.data)


class Lagoon2:
    """Efficient lagoon.
    
    Somehow track all this info sparsely.
    """
    def __init__(self, inclusive=False):
        # should segments be inclusive? Sure, for now.
        self.segments = []
        self.row_min = 0
        self.row_max = 0 # inclusive
        self.col_min = 0
        self.col_max = 0 # inclusive
        self.pos = (0, 0)
        self._filled = False
        self._inclusive = inclusive

    def dig(self, dig_line):
        r, c = self.pos
        dr, dc = {"R": (0, 1), "L": (0, -1), "U": (-1, 0), "D": (1, 0)}[
            dig_line.direction
        ]
        if self._inclusive:
            # exclusive end
            rn, cn = r + (dig_line.length + 1) * dr, c + (dig_line.length + 1) * dc
        else:
            # inclusive end
            rn, cn = r + (dig_line.length) * dr, c + (dig_line.length) * dc        
        self._ensure_space(rn, cn)
        self.segments.append(((r, c), (rn, cn)))
        self.pos = (rn, cn)
    
    def dig_all(self, dig_lines):
        for dig_line in dig_lines:
            self.dig(dig_line)

    def _ensure_space(self, r, c):
        self.row_min = min(r, self.row_min)
        self.row_max = max(r, self.row_max)
        self.col_min = min(r, self.col_min)
        self.col_max = max(r, self.col_max)

    def fill(self):
        self._filled = True

    def count_holes(self):
        # shoelace theorem!
        sum = 0
        for seg in self.segments:
            (r1, c1), (r2, c2) = seg
            # r2 += 1; c2 += 1
            # sum += r1 * c2 - r2 * c1
            sum += c1 * r2 - c2 * r1
        # Count the boundary.
        # This is ugly, forget this.
        for (r1, c1), (r2, c2) in self.segments:
           sum += abs(c2 - c1) if r1 == r2 else abs(r2 - r1)
        return sum * 0.5 + 1
    
# 3x3 square
test_dp = [
    DigLine('R', 2, ''),
    DigLine('D', 2, ''),
    DigLine('L', 2, ''),
    DigLine('U', 2, ''),
]

"""
dp = read_dig_plan('test_data.txt')
dp2 = [reparse(x) for x in dp]
dp_full = read_dig_plan('data.txt')
dp_full2 = [reparse(x) for x in dp_full]
"""