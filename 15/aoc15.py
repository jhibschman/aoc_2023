"""
Advent of Code, day 15.
"""


def aoc_hash(s):
    bs = s.encode()
    x = 0
    for b in bs:
        x = (x + b) * 17 % 256
    return x


def parse_steps(data):
    data = data.strip()
    return data.split(",")


def hash_total(steps):
    return sum(aoc_hash(s) for s in steps)


# For once, it's actually easier to just make a class.


class Box:
    def __init__(self, box_num):
        self.box_num = box_num
        self.lenses = []  # [label, power] pairs

    def set(self, label, power):
        vs = [lp for lp in self.lenses if lp[0] == label]
        if vs:
            vs[0][1] = power
        else:
            self.lenses.append([label, power])

    def rem(self, label):
        self.lenses = [lp for lp in self.lenses if lp[0] != label]

    def power(self):
        tot = 0
        for i, (_, p) in enumerate(self.lenses):
            tot += (self.box_num + 1) * (i + 1) * p
        return tot


class Boxes:
    def __init__(self):
        self.boxes = [Box(i) for i in range(256)]

    def print(self):
        for i, b in enumerate(self.boxes):
            if b.lenses:
                print(f"{i:3d}: {b.lenses}")

    def do_data(self, data):
        for step in parse_steps(data):
            self.do(step)

    def do(self, step):
        if step.endswith("-"):
            label = step[:-1]
            self.boxes[aoc_hash(label)].rem(label)
        else:
            parts = step.split("=")
            label, power = parts[0], int(parts[1])
            self.boxes[aoc_hash(label)].set(label, power)

    def power(self):
        return sum(b.power() for b in self.boxes)


def part1():
    return hash_total(parse_steps(open("data.txt").read()))


def part2():
    b = Boxes()
    b.do_data(open("data.txt").read())
    return b.power()
