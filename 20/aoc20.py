"""
Advent of Code, day 20.

Signal propagation.
"""

import collections

def read_network(path):
    with open(path) as f:
        return parse_network(f)


def parse_network(input):
    nodes = {}
    for line in input:
        line = line.strip()
        src, dest_line = line.split(' -> ')
        dests = [x.strip() for x in dest_line.split(',')]
        name = src if src == 'broadcaster' else src[1:] 
        if src == 'broadcaster':
            nodes[name] = Broadcast(name, dests)
        elif src.startswith('%'):
            nodes[name] = FlipFlop(name, dests)
        elif src.startswith('&'):
            nodes[name] = Conjunction(name, dests)
        else:
            raise RuntimeError('parse error')
    return Network(nodes)


class Network:
    def __init__(self, node_dict):
        self.button = Button()
        if 'broadcaster' not in node_dict:
            raise ValueError('no broadcaster')
        self.node_dict = node_dict
        self._create_dummies()
        self._update_inputs()

    def _update_inputs(self):
        for d in self.node_dict.values():
            d.reset_inputs()
        for d in self.node_dict.values():
            for dest_name in d.dests:
                self.node_dict[dest_name].add_input(d.name)

    def _create_dummies(self):
        missing = set()
        for node in self.node_dict.values():
            for dest in node.dests:
                if dest not in self.node_dict:
                    missing.add(dest)
        print(missing)
        for name in missing:
            self.node_dict[name] = Dummy(name, [])

    def push_button(self, bus):
        self.button.activate(bus)

    def get(self, node_name):
        return self.node_dict[node_name]


class Bus:
    def __init__(self, verbose=False):
        self.queue = collections.deque()
        self.low_count = 0
        self.high_count = 0
        self.verbose = verbose

    def send(self, src, dest, signal):
        if signal == 'low':
            self.low_count += 1
        else:
            self.high_count += 1
        self.queue.append((src, dest, signal))
        if self.verbose:
            print(f'{src} -{signal}-> {dest}')

    def step(self, network):
        """Run a step of the bus."""
        if not self.queue:
            return False
        # src, dest, sig = self.queue.popleft()
        tp = self.queue.popleft()
        network.get(tp[1]).receive(self, tp[0], tp[2])
        return tp

    def run(self, network):
        while self.step(network):
            pass


class Button:
    def activate(self, bus):
        bus.send('button', 'broadcaster', 'low')


class Node:
    def __init__(self, name, dests):
        self.name = name
        self.dests = dests
        self.inputs = []

    def reset_inputs(self):
        self.inputs = []

    def add_input(self, inp):
        self.inputs.append(inp)

    def _send(self, bus, signal):
        for d in self.dests:
            bus.send(self.name, d, signal)


class Broadcast(Node):
    def receive(self, bus, sender, signal):
        del sender
        self._send(bus, signal)


class FlipFlop(Node):
    def __init__(self, name, dests):
        super().__init__(name, dests)
        self.is_on = False
    
    def receive(self, bus, sender, signal):
        del sender
        if signal == 'high':
            return
        self._send(bus, 'low' if self.is_on else 'high')
        self.is_on = not self.is_on


class Dummy(Node):
    def receive(self, bus, sender, signal):
        del bus, sender, signal
        pass


class Conjunction(Node):
    def __init__(self, name, dests):
        super().__init__(name, dests)
        self.memory = collections.defaultdict(lambda: 'low')

    def receive(self, bus, sender, signal):
        self.memory[sender] = signal
        # print('conj inputs:', self.inputs, dict(self.memory))
        if all(self.memory[inp] == 'high' for inp in self.inputs):
            self._send(bus, 'low')
        else:
            self._send(bus, 'high')


def score(bus):
    return bus.low_count * bus.high_count

def part1(path, times, verbose=False):
    net = read_network(path)
    bus = Bus(verbose=verbose)
    for _ in range(times):
        net.push_button(bus)
        bus.run(net)
    return score(bus)


def part1_test(once=False, verbose=False):
    return part1('test_data.txt', 1 if once else 1000, verbose)

def part1_test2(once=False, verbose=False):
    return part1('test_data2.txt', 1 if once else 1000, verbose)

def do_part1():
    return part1('data.txt', 1000)

def part2(path, limit=1_000_000, verbose=False):
    # This is wrong, it just terminates the loop.
    net = read_network(path)
    bus = Bus(verbose=verbose)
    net.push_button(bus)
    count = 0
    while res := bus.step(net):
        count += 1
        if res == ('rx', 'low'):
            break
        if count >= limit:
            raise RuntimeError('limit exceeded')
    return count


# Brute force doesn't work, need to look at the cycle lengths on the
# inputs to the final node.
# Answer was 221_453_937_522_197, so there's no hope of looping to
# get there.
def part2_bus(path, limit=100_000_000, verbose=False, scan=('rx', 'low')):
    net = read_network(path)
    bus = Bus(verbose=verbose)
    button_count = 0
    while button_count < limit:
        button_count += 1
        net.push_button(bus)
        step_count = 0
        while res := bus.step(net):
            step_count += 1
            if step_count % 1_000 == 0:
                print(f'button: {button_count}, step: {step_count}')
            if res[1] == scan[0] and res[2] == scan[1]:
                return button_count
        if button_count % 500_000 == 0:
            print(f'button: {button_count}, final step: {step_count}')
    return bus

def do_part2():
    return part2('data.txt', verbose=True)

"""
Analysis:

In [5]: net.get('rx').inputs
Out[5]: ['hf']

In [6]: net.get('hf')
Out[6]: <aoc20.Conjunction at 0x111043460>

In [7]: net.get('hf').inputs
Out[7]: ['nd', 'pc', 'vd', 'tx']

In [18]: aoc20.part2_bus('data.txt', scan=('vd', 'low'))
{'rx'}
Out[18]: 3767

In [19]: aoc20.part2_bus('data.txt', scan=('nd', 'low'))
{'rx'}
Out[19]: 4019

In [20]: aoc20.part2_bus('data.txt', scan=('pc', 'low'))
{'rx'}
Out[20]: 3881

In [21]: aoc20.part2_bus('data.txt', scan=('tx', 'low'))
{'rx'}
Out[21]: 3769

In [22]: 3767*4019*3881*3769
Out[22]: 221453937522197

TODO: find a way to make this systematic.
"""
